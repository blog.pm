Чтобы блог заработал вам понадобится шелл :)

Подключаемся к серверу и настраиваем cpan:

    $ ssh example.com
    $ perl -MCPAN -e shell

В конфигурационный файл своего шелла (.bashrc, .zshrc…) добавляем:

    export PERL5LIB="$HOME/lib/perl"

Перезаходим или же сообщаем шеллу перечитать конфигурационный файл и добавляем
в .cpan/CPAN/MyConfig.pm

    'makepl_arg' => qq[PREFIX=~/ LIB=~/lib/perl],

Предположим, что блог будет храниться в /var/www/example.com. Переходим туда и
получаем свежую версию.

    $ cd /var/www/example.com
    $ git clone http://git.godcore.org.ua/blog.git

Запускаем на установку необходимые модули:

    $ perl Makefile.PL
    $ make

Все установится в вашу домашнюю директорию.

Копируем файл с настройками blog.yml.example в blog.yml и вносим
соответствующие изменения.

Для проверки запускаем встроенный сервер:

    $ script/blog_server.pl -d

Он запускается на 3000 порту, поэтому можно попробовать http://example.com:3000.
