#!/usr/bin/perl

use strict;

use DBI;

my ( $db_from, $db_to ) = @ARGV;

die "usage: $0 <from> <to>" unless -f $db_from && $db_to;

my $dbh_from = DBI->connect( "dbi:SQLite:$db_from" ) or die $DBI::errstr;
my $dbh_to   = DBI->connect( "dbi:SQLite:$db_to" )   or die $DBI::errstr;

my $posts = $dbh_from->selectall_arrayref( 'SELECT * FROM post' );
foreach my $post ( @$posts ) {
    my ( $id, $addtime, $title, $annot, $content ) = @$post;
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO post(id,key,addtime,modtime,title,annot,content)
        VALUES(?, ?, ?, ?, ?, ?, ?)
        /
    );
    $sth->execute( $id, $id, $addtime, $addtime, $title, $annot, $content )
      or warn $dbh_to->errstr;
}

my $tags = $dbh_from->selectall_arrayref( 'SELECT * FROM tag' );
foreach my $tag ( @$tags ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO tag VALUES(?, ?)
        /
    );
    $sth->execute( @$tag )
      or warn $dbh_to->errstr;
}

my $tag_maps = $dbh_from->selectall_arrayref( 'SELECT * FROM post_tag_map' );
foreach my $tag_map ( @$tag_maps ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO post_tag_map VALUES(?, ?)
        /
    );
    $sth->execute( @$tag_map )
      or warn $dbh_to->errstr;
}

my $comments = $dbh_from->selectall_arrayref( 'SELECT * FROM comment' );
foreach my $comment ( @$comments ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO comment VALUES(?, ?, ?, ?, ?, ?, ?, ?)
        /
    );
    $sth->execute( @$comment )
      or warn $dbh_to->errstr;
}

my $pages = $dbh_from->selectall_arrayref( 'SELECT * FROM page' );
foreach my $page ( @$pages ) {
    my ( $id, $addtime, $name, $content ) = @$page;
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO page(id,key,addtime,modtime,title,content)
        VALUES(?, ?, ?, ?, ?, ?)
        /
    );
    $sth->execute( $id, $id, $addtime, $addtime, $name, $content )
      or warn $dbh_to->errstr;
}

$dbh_from->disconnect();
$dbh_to->disconnect();
