#!/usr/bin/perl

use strict;

use DBI;

my ( $default_lang, $db_from, $db_to ) = @ARGV;

my @languages = (qw/ ru en /);

die "usage: $0 <default-lang> <from> <to>"
  unless $default_lang && -f $db_from && $db_to;

my $dbh_from = DBI->connect( "dbi:SQLite:$db_from" ) or die $DBI::errstr;
my $dbh_to   = DBI->connect( "dbi:SQLite:$db_to" )   or die $DBI::errstr;

my $posts = $dbh_from->selectall_arrayref( 'SELECT * FROM post ORDER BY id ASC' );
foreach my $post ( @$posts ) {
    my ( $id, $key, $addtime, $modtime, $title, $annot, $content ) = @$post;
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO post(id,key,addtime,modtime,orig_lang)
        VALUES(?, ?, ?, ?, ?)
        /
    );
    $sth->execute( $id, $key, $addtime, $modtime, $default_lang )
      or warn $dbh_to->errstr;

    $content =~ s/\Q$annot\E/$annot\n\[cut\]\n/s;
    foreach my $lang ( @languages ) {
        my $sth = $dbh_to->prepare(
            qq/
            INSERT INTO post_i18n(post_id,lang,istran,title,annot,content)
            VALUES (?, ?, ?, ?, ?, ?)
            /
        );
        $sth->execute( $id, $lang, 0, $title, $annot, $content );
    }
}

my $tags = $dbh_from->selectall_arrayref( 'SELECT * FROM tag' );
foreach my $tag ( @$tags ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO tag VALUES(?, ?)
        /
    );
    $sth->execute( @$tag )
      or warn $dbh_to->errstr;
}

my $tag_maps = $dbh_from->selectall_arrayref( 'SELECT * FROM post_tag_map' );
foreach my $tag_map ( @$tag_maps ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO post_tag_map VALUES(?, ?)
        /
    );
    $sth->execute( @$tag_map )
      or warn $dbh_to->errstr;
}

my $comments = $dbh_from->selectall_arrayref( 'SELECT * FROM comment' );
foreach my $comment ( @$comments ) {
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO comment VALUES(?, ?, ?, ?, ?, ?, ?, ?)
        /
    );
    $sth->execute( @$comment )
      or warn $dbh_to->errstr;
}

my $pages = $dbh_from->selectall_arrayref( 'SELECT * FROM page ORDER BY id ASC' );
foreach my $page ( @$pages ) {
    my ( $id, $key, $addtime, $modtime, $title, $content ) = @$page;
    my $sth = $dbh_to->prepare(
        qq/
        INSERT INTO page(id,key,addtime,modtime,orig_lang)
        VALUES(?, ?, ?, ?, ?)
        /
    );
    $sth->execute( $id, $key, $addtime, $modtime, $default_lang )
      or warn $dbh_to->errstr;

    foreach my $lang ( @languages ) {
        my $sth = $dbh_to->prepare(
            qq/
            INSERT INTO page_i18n(page_id,lang,istran,title,content)
            VALUES (?, ?, ?, ?, ?)
            /
        );
        $sth->execute( $id, $lang, 0, $title, $content );
    }
}

$dbh_from->disconnect();
$dbh_to->disconnect();
