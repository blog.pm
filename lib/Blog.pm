package Blog;

use strict;

use Catalyst::Runtime '5.70';

use Catalyst::Authentication::Store::Minimal;

use Blog::NewDB;
use Common::Unicode;

use Catalyst qw/
    Unicode

    ConfigLoader

    Static::Simple

    Session
    Session::Store::FastMmap
    Session::State::Cookie

    Authentication
    +Common::Plugin::Authorization::ACL::Paranoid

    I18N

    CustomErrorMessage

    Singleton
    Assets

    +CatalystX::Plugin::I18N::URI
  /;

our $VERSION = '0.04';

__PACKAGE__->dispatcher_class( 'Common::Dispatcher::LangDetect' );

__PACKAGE__->config(
    name             => 'Blog',
    style            => 'default',
    'Plugin::Assets' => {
        path        => '/static',
        output_path => 'built/',
        minify      => 1
    }
);

__PACKAGE__->setup;

__PACKAGE__->acl(
    rules => {
        '/index' => 1,

        '/user/login'        => sub { !shift->user_exists() },
        '/user/login_openid' => sub { !shift->user_exists() },
        '/user/logout'       => sub { shift->user_exists() },

        '/post/list'         => 1,
        '/post/view'         => 1,
        '/post/feed'         => 1,
        '/post/archive'      => 1,
        '/post/archive_list' => 1,

        '/page/view'         => 1,

        '/tag/list' => 1,
        '/tag/view' => 1,

        '/comment/add'  => 1,
        '/comment/feed' => 1,

        '*' => sub { shift->user_in_realm( 'admins' ) }
    }
);

__PACKAGE__->log->disable( 'debug' ) if !__PACKAGE__->debug;

=head1 NAME

Blog - Catalyst based application

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
