package Blog::Controller::Comment;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Blog::Form::Comment;
use Blog::Validator::Comment;
use HTTP::Date;

sub add : Local Args(0) {
    my ( $self, $c ) = @_;

    my $post;

    if ( my $post_id = $c->req->param( 'post_id' ) ) {
        $post = $c->model( 'Post' )->create( id => $post_id );
    } else {
        $self->throw_not_found( $c );
    }

    $post->load(
        query => [ 't2.lang' => $c->language ],
        with  => [ 'post_i18n' ],

        #select      => [ 'id', 'key', 'title' ],
        speculative => 1
    );

    $self->throw_not_found( $c ) if $post->not_found;

    $c->stash->{ post } = {
        id    => $post->id,
        key   => $post->key,
        title => $post->i18n->title
    };

    my $form = Blog::Form::Comment->new( $c );

    my $validator = Blog::Validator::Comment->new( $c->req->params );

    if ( $form->was_submitted && $validator->is_valid ) {
        my $object =
          $form->create_from_form( $c->model( 'Comment' )->name->new(),
            $validator->params );
        $object->save();

        $c->flash->{ msg } = $c->loc( 'Comment was added sucessfully' );

        $c->res->redirect( $c->uri_for( '/post/view', $post->key ) );
    }

    $c->stash->{ validator } = $validator;
}

sub feed : Local Args(0) {
    my ( $self, $c ) = @_;

    my $items = $c->model( 'Comment' )->search(
        query => ['t3.lang' => $c->language],
        #select =>
          #[ qw/ id addtime openid name isauthor post_id content t2.title t2.key / ],
        sort_by         => 'addtime DESC',
        limit           => 20,
        require_objects => [ 'post', 'post.post_i18n' ],
        group_by => 't1.post_id'
    );

    $c->stash->{ feed } = {
        description => $c->loc( 'Recent comments' ),
        items       => []
    };

    return unless @$items;

    my $feed_items =  [
        map {
            my $author =
                $_->isauthor
              ? $c->config->{ author } . ' (' . $c->loc( 'Author' ) . ')'
              : $_->openid ? $c->parse_openid( $_->openid )->{ name }
              :              ( $_->name || $c->loc( 'Anonymous' ) );
            {
                title =>
                  $c->loc( '[_1] on post "[_2]"', $author, $_->post->i18n->title ),
                description => $c->ccode( $_->content ),
                pubDate     => $_->addtime->epoch,
                link        => $c->uri_for( '/post/view', $_->post->key )
                  . '#comment-'
                  . $_->id,
                author => $author
            }
          } @$items
    ];

    $c->res->headers->header(
        'Last-Modified' => time2str( $feed_items->[ 0 ]->{ pubDate } ) );

    $c->stash->{ feed }->{ items } = $feed_items;
}

sub delete : LocalRegex('^delete/(\d+)$') {
    my ( $self, $c ) = @_;

    my $comment =
      $c->model( 'Comment' )->create( id => $c->req->captures->[ 0 ] );
    $comment->load( speculative => 1, select => [ 'id' ] );
    $self->throw_not_found( $c ) if $comment->not_found;

    $comment->delete;

    $c->flash->{ msg } = $c->loc( 'Comment was deleted sucessfully' );

    $c->res->redirect( $c->uri_for( '/post/view', $comment->post->key ) );
}

sub throw_not_found {
    my ( $self, $c ) = @_;

    $c->detach( '/default' );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
