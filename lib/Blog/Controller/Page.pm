package Blog::Controller::Page;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Blog::Form::Page;
use Blog::Validator::Page;

sub view : Local Args(1) {
    my ( $self, $c, $id ) = @_;

    my $page = $c->model( 'Page' )->create( key => $id );
    $page->load(
        query       => [ 't2.lang' => $c->language ],
        with        => [ 'page_i18n' ],
        speculative => 1
    );

    $self->throw_not_found( $c ) if $page->not_found;

    $c->stash->{ page } = {
        $page->column_value_pairs,
        is_original => $page->is_original,
        available_translations => $page->available_translations
    };
}

sub add : Local Args(0) {
    my ( $self, $c ) = @_;

    my $form      = Blog::Form::Page->new( $c );
    my $validator = Blog::Validator::Page->new( $c->req->params );

    if ( $form->was_submitted && $validator->is_valid ) {
        my $page = $form->create_from_form( $c->model( 'Page' )->name->new(),
            $validator->params );
        $page->save();

        $c->flash->{ msg } = $c->loc( 'Page was added successfully' );

        $c->res->redirect( $c->uri_for( '/page/view', $page->key ) );
    }

    $c->stash->{ validator } = $validator;
}

sub edit : LocalRegex('^edit/(\d+)$') {
    my ( $self, $c ) = @_;

    my $id = $c->req->captures->[ 0 ];

    my $page = $c->model( 'Page' )->create( id => $id );
    $page->load(
        query => [ 't2.lang' => $c->language ],
        with  => [ 'page_i18n' ],
        speculative => 1
    );
    $self->throw_not_found( $c ) if $page->not_found;

    my $form      = Blog::Form::Page->new( $c );
    my $validator = Blog::Validator::Page->new( $c->req->params );

    if (   $form->was_submitted
        && $validator->is_valid( unique_except => $page->key ) )
    {
        $page = $form->update_from_form( $page, $validator->params );
        $page->save( cascade => 1 );

        $c->flash->{ msg } = $c->loc( 'Page was saved successfully' );

        $c->res->redirect( $c->uri_for( '/page/view', $page->key ) );
    }

    $c->stash->{ validator } = $validator;
    $c->stash->{ page }      = { $page->column_value_pairs };
}

sub delete : LocalRegex('^delete/(\d+)$') {
    my ( $self, $c ) = @_;

    my $post = $c->model( 'Page' )->create( id => $c->req->captures->[ 0 ] );
    $post->load( speculative => 1, select => [ 'id' ] );
    $self->throw_not_found( $c ) if $post->not_found;

    $post->delete( cascade => 1 );

    $c->flash->{ msg } = $c->loc( 'Page was deleted successfully' );

    $c->res->redirect( $c->uri_for( '/' ) );
}

sub throw_not_found {
    my ( $self, $c ) = @_;

    $c->detach( '/default' );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
