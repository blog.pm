package Blog::Controller::Post;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Blog::Form::Post;
use Blog::Validator::Post;

use HTTP::Date;

sub list : Path('') Args(0) { }

sub archive
    : Route('archive/:year|(19|20)\d\d|=/:month|(0|1)?\d|=/:day|[0-3]?\d|=')
#sub archive_list : LocalRegex('archive/((?:19|20)?\d\d)/(?:((?:0|1)?\d)/())?')
{
    my ( $self, $c ) = @_;

    foreach ( qw/ year month day / ) {
        $c->stash->{ $_ } = $c->req->param( $_ );
    }
}

sub view : Local Args(1) {
    my ( $self, $c, $id ) = @_;

    my $post = $c->model( 'Post' )->create( key => $id );
    $post->load( i18n => $c->language,
        speculative => 1
    );

    $self->throw_not_found( $c ) if $post->not_found;

    $c->stash->{ post } = {
        $post->column_value_pairs,
        available_translations => $post->available_translations,
        tags => [ $post->tags ]
    };
}

sub add : Local Args(0) {
    my ( $self, $c ) = @_;

    my $form      = Blog::Form::Post->new( $c );
    my $validator = Blog::Validator::Post->new( $c->req->params );

    if ( $form->was_submitted && $validator->is_valid ) {
        my $post = $form->create_from_form( $c->model( 'Post' )->name->new(),
            $validator->params );
        $post->save();

        $c->flash->{ msg } = $c->loc( 'Post was added successfully' );

        $c->res->redirect( $c->uri_for( '/post/view', $post->key ) );
    }

    $c->stash->{ validator } = $validator;
}

sub edit : LocalRegex('^edit/(\d+)$') {
    my ( $self, $c ) = @_;

    my $id = $c->req->captures->[ 0 ];

    my $post = $c->model( 'Post' )->create( id => $id );
    $post->load( i18n => $c->language,
        speculative => 1
    );
    $self->throw_not_found( $c ) if $post->not_found;

    my $form      = Blog::Form::Post->new( $c );
    my $validator = Blog::Validator::Post->new( $c->req->params );

    if (   $form->was_submitted
        && $validator->is_valid( unique_except => $post->key ) )
    {
        $post = $form->update_from_form( $post, $validator->params );
        $post->save( cascade => 1);

        $c->flash->{ msg } = $c->loc( 'Post was saved successfully' );

        $c->res->redirect( $c->uri_for( '/post/view', $post->key ) );
    }

    $c->stash->{ validator } = $validator;

    $c->stash->{ post } = {
        $post->column_value_pairs,
        tags => [ $post->tags ]
    };
}

sub delete : LocalRegex('^delete/(\d+)$') {
    my ( $self, $c ) = @_;

    my $object = $c->model( 'Post' )->create( id => $c->req->captures->[ 0 ] );
    $object->load( speculative => 1, select => [ 'id' ] );
    $self->throw_not_found( $c ) if $object->not_found;

    $object->delete( cascade => 1 );

    $c->flash->{ msg } = $c->loc( 'Post was deleted successfully' );

    $c->res->redirect( $c->uri_for( '/post' ) );
}

sub feed : Local Args(0) {
    my ( $self, $c ) = @_;

    my $items = $c->model( 'Post' )->manager->feed();

    $c->stash->{ feed } = {
        description => $c->loc( 'Recent posts' ),
        items       => []
    };

    return unless @$items;

    my $feed_items = [
        map {
            {
                title       => $_->i18n->title,
                description => $c->helper('BBCode')->render( $_->i18n->annot ),
                category    => [ map { { name => $_->name } } @{ $_->tags } ],
                pubDate     => $_->addtime->epoch,
                link     => $c->uri_for( '/post/view', $_->key ),
                comments => $c->uri_for( '/post/view', $_->key ) . '#comments',
                author   => $c->config->{ author }
            }
          } @$items
    ];

    $c->res->headers->header(
        'Last-Modified' => time2str( $feed_items->[ 0 ]->{ pubDate } ) );

    $c->stash->{ feed }->{ items } = $feed_items;
}

sub throw_not_found {
    my ( $self, $c ) = @_;

    $c->detach( '/default' );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
