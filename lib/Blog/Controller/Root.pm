package Blog::Controller::Root;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Time::HiRes qw( gettimeofday tv_interval );

__PACKAGE__->config->{ namespace } = '';

sub begin : Private {
    my ( $self, $c ) = @_;

    $c->stash->{ user_exists } = 1 if $c->user_exists;

    $c->stash->{ user_is_admin } = 1 if $c->user_in_realm( 'admins' );

    $c->stash->{ start_time } = [ gettimeofday ];
}

sub index : Path('/') Args(0) {
    my ( $self, $c ) = @_;

    $c->forward( '/post/list' );

    $c->stash->{ template } = 'post/list.tt2';
}

sub forbidden : Private {
    my ( $self, $c ) = @_;

    $c->res->status( '403' );
    $c->error( 'Access denied' );
}

sub default : Private {
    my ( $self, $c ) = @_;

    $c->res->status( '404' );
    $c->error( 'Not found' );
}

sub end : ActionClass('RenderView') {
    my ( $self, $c ) = @_;

    $c->stash->{ elapsed_time } =
      tv_interval( $c->stash->{ start_time }, [ gettimeofday ] );

    if ( scalar @{ $c->error } ) {
        $c->stash->{ errors }   = $c->error;
        $c->stash->{ template } = 'error.tt2';
        $c->res->status( 500 ) unless $c->res->status;
        $c->forward( 'Blog::View::TT' );
        $c->error( 0 );
    } else {
        if ( my $feed = $c->stash->{ feed } ) {
            $c->res->content_type( 'application/rss+xml; charset=utf-8' );
        }
        #$c->cache_page unless $c->user_exists || $c->stash->{ no_cache };
    }
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
