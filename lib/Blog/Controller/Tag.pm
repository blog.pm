package Blog::Controller::Tag;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Blog::Form::Post;

sub list : Path('') Args(0) { }

sub view : LocalRegex('^view/(\d+)$') {
    my ( $self, $c ) = @_;

    my $tag = $c->model( 'Tag' )->create( id => $c->req->captures->[ 0 ] );
    $tag->load( speculative => 1 );

    $self->throw_not_found( $c ) if $tag->not_found;

    $c->stash->{ tag } = $tag;
}

sub delete : LocalRegex('^delete/(\d+)$') {
    my ( $self, $c ) = @_;

    my $object = $c->model( 'Tag' )->create( id => $c->req->captures->[ 0 ] );
    $object->load( speculative => 1, select => [ 'id' ] );
    $self->throw_not_found( $c ) if $object->not_found;

    $object->delete;

    $c->flash->{ msg } = $c->loc( 'Tag was deleted sucessfully' );

    $c->res->redirect( $c->uri_for( '/tag' ) );
}

sub throw_not_found {
    my ( $self, $c ) = @_;

    $c->detach( '/default' );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
