package Blog::Controller::User;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Blog::Form;
use Blog::Validator::Login;
use Blog::Validator::LoginOpenID;
use Encode;

sub login : Global Args(0) {
    my ( $s, $c ) = @_;

    my $form = Blog::Form->new( $c );
    my $validator = Blog::Validator::Login->new( $c->req->params );

    if (   !$form->was_submitted
        && $c->req->referer
        && $c->req->referer ne $c->req->uri
        && $c->req->referer !~ m/login/o )
    {
        $c->session->{ referer } = $c->req->referer;
    }

    if ( $form->was_submitted && $validator->is_valid ) {
        if (
            $c->authenticate(
                {
                    username => $validator->param( 'name' ),
                    password => encode_utf8( $validator->param( 'password' ) ),
                }
            )
          )
        {
            $c->flash->{ msg } = $c->loc('Login was successfull');

            my $referer = $c->session->{ referer };
            $c->session->{ referer } = undef;
            $c->res->redirect( $referer || $c->uri_for( '/' ) );
        } else {
            $c->forward('/forbidden');
        }
    }

    $c->stash->{ validator } = $validator;
    $c->stash->{ template } = 'login.tt2';
}

sub login_openid : Global Args(0) {
    my ( $s, $c ) = @_;

    my $loggedin = 0;

    eval { $loggedin = $c->authenticate( {}, 'openid' ) };

    $c->detach('/forbidden') if $@;

    if ( $loggedin ) {
        $c->flash->{ msg } = $c->loc('OpenID login was successfull');
        $c->res->redirect( $c->uri_for( '/' ) );
    }

    my $form = Blog::Form->new( $c );
    my $validator = Blog::Validator::LoginOpenID->new( $c->req->params );

    $validator->is_valid() if $form->was_submitted;

    $c->stash->{ validator } = $validator;
    $c->stash->{ template } = 'login_openid.tt2';
}

sub logout : Local Args(0) {
    my ( $self, $c ) = @_;

    $c->logout;

    undef $c->session->{ user_roles };

    $c->flash->{ msg } = $c->loc('Logout was successfull');

    return $c->res->redirect( $c->req->referer || $c->uri_for( '/' ) );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
