package Blog::DB::Object;

use strict;
use warnings;
use utf8;

use base qw( Rose::DB::Object Rose::DB::UTF8Columns );

use Blog::DB;

sub init_db { shift; Blog::DB->new_or_cached( @_ ) }

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

