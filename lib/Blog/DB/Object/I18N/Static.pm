package Blog::DB::Object::I18N::Static;

use strict;

use base qw/ Rose::DB::Object::I18N::Static Rose::DB::UTF8Columns / ;

use Blog::DB;

sub init_db { shift; Blog::DB->new_or_cached( @_ ) }

sub languages { 
    my @languages = keys %{ Blog->config->{ languages } };

    wantarray ? @languages : \@languages;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
