package Blog::Form;

use strict;

sub new {
    my ( $class, $c ) = @_;

    my $self = bless { c => $c }, $class;

    return $self;
}

sub was_submitted {
    my ( $self, $param ) = @_;

    $param ||= 'submit';

    if ( $self->{ c }->req->param($param) ) {
        return 1;
    }

    return 0;
}

sub create_from_form {
    my ( $self ) = shift;
    my ( $object, $params ) = @_;

    while ( my ($field, $value) = each %$params ) {
        if ( $object->can( $field ) ) {
            $object->$field( $value );
        }
    }

    if ( $object->isa( 'Blog::DB::Object::I18N::Static' ) ) { 
        my $c = Blog->context;
        $object->orig_lang( $params->{ language } );
        $object->languages( keys %{ $c->config->{ languages } } );

        my $rel_name = $object->get_i18n_rel_name();

        my $i18n = $object->meta->relationship($rel_name)->class->new();

        my $values = { istran => 0 };
        while ( my ($field, $value) = each %$params ) {
            if ( $i18n->can( $field ) ) { 
                $values->{ $field } = $value;
            }
        }
        $object->$rel_name($values);
    }

    return $object;
}

sub update_from_form {
    my ( $self ) = shift;
    my ( $object, $params ) = @_;

    while ( my ($field, $value) = each %$params ) {
        if ( $object->can( $field ) ) {
            $object->$field( $value );
        }
    }

    if ( $object->isa( 'Blog::DB::Object::I18N::Static' ) ) { 
        my $c = Blog->context;
        $object->languages( keys %{ $c->config->{ languages } } );

        my $i18n =
            $object->i18n
          ? $object->i18n
          : $object->i18n( [ lang => $params->{language} ] );

        return $object unless $i18n;

        while ( my ($field, $value) = each %$params ) {
            if ( $i18n->can( $field ) ) { 
                $object->i18n->$field( $value );
            }
        }
    }

    return $object;
}

#sub init_from_object {
    #my $self = shift;
    #my ( $object ) = @_;

    #foreach my $field ( $self->fields ) {
        #my $name = $field->local_name;

        #if ( $object->can( $name ) ) {
            #$field->input_value( scalar $object->$name );
        #}
    #}
#}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
