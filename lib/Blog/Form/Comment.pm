package Blog::Form::Comment;

use base 'Blog::Form';

sub create_from_form {
    my ( $self ) = shift;
    my ( $object, $params ) = @_;

    $object = $self->SUPER::create_from_form( @_ );

    my $c = $self->{ c };
    $object->isauthor( $c->user_in_realm( 'admins' ) ? 1 : 0 );
    $object->openid( $c->user->url ) if $c->user_in_realm( 'openid' );

    return $object;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
