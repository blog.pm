package Blog::Form::Page;

use base 'Blog::Form';

sub create_from_form {
    my ( $self ) = shift;

    my $object = $self->SUPER::create_from_form( @_ );

    return $object;
}

sub update_from_form {
    my ( $self ) = shift;

    my $object = $self->SUPER::update_from_form( @_ );

    return $object;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
