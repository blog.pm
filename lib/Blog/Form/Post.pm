package Blog::Form::Post;

use base 'Blog::Form';

sub create_from_form {
    my ( $self ) = shift;
    my ( $class, $params ) = @_;

    my $object = $self->SUPER::create_from_form( @_ );

    $self->_set_tags( $object, $params->{ tag_names } );

    return $object;
}

sub update_from_form {
    my ( $self ) = shift;
    my ( $class, $params ) = @_;

    my $object = $self->SUPER::update_from_form( @_ );

    $self->_set_tags( $object, $params->{ tag_names } );

    return $object;
}

sub _set_tags {
    my ( $self, $object, $tags_field_value ) = @_;

    if ( $tags_field_value ) {
        $tags_field_value =~ s/^\s+//go;
        $tags_field_value =~ s/\s+$//go;
        $tags_field_value =~ s/\s{2,}/ /go;
        $tags_field_value =~ s/\s(?:\,)/,/go;
        $tags_field_value =~ s/(?:\,)\s/,/go;
        $tags_field_value =~ s/\,{2,}/,/go;

        my @tag_names = split( ',', $tags_field_value );

        my @tags = map {
            { +name => $_ }
        } @tag_names;

        $object->tags( @tags );
    } else {
        $object->tags( [] );
    }
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
