package Blog::I18N::ru;

use strict;
use utf8;

use base qw/ Blog::I18N /;

our %Lexicon = (
    'Available translations' => 'Доступные переводы',
    'Add comment'         => 'Добавить комментарий',
    'Add page'            => 'Добавить страницу',
    'Add post'            => 'Добавить запись',
    'Add'                 => 'Добавить',
    'all tags'            => 'все тэги',
    'Annotation'          => 'Аннотация',
    'Anonymous'           => 'Аноним',
    'Archive'             => 'Архив',
    'archive'             => 'архив',
    'Author'              => 'Автор',
    'Back'                => 'Назад',
    'Blog'                => 'Блог',
    'Comments'            => 'Комментарии',
    'Content'             => 'Содержимое',
    'Delete'              => 'Удалить',
    'Earlier'             => 'Раньше',
    'Edit page'           => 'Редактировать страницу',
    'Edit post'           => 'Редактировать запись',
    'Edit'                => 'Редактировать',
    'Edit'                => 'Редактировать',
    'Empty tags'          => 'Пустые тэги',
    'Error'               => 'Ошибка',
    'Key'                 => 'Ключ',
    'Later'               => 'Позже',
    'Leave a comment'     => 'Оставить комментарий',
    'Login with OpenID'   => 'Войти с помощью OpenID',
    'Login'               => 'Вход',
    'Logout'              => 'Выход',
    'Name'                => 'Имя',
    'Password'            => 'Пароль',
    'Posts'               => 'Записи',
    'Rank'                => 'Порядок',
    'Recent comments'     => 'Недавние комментарии',
    'Recent posts'        => 'Недавние записи',
    'Save'                => 'Сохранить',
    'Subscribe'           => 'Подписка',
    'Tags'                => 'Тэги',
    'Title'               => 'Заголовок',
    'Won\'t be published' => 'Не будет опубликован',
    'Add translation' => 'Добавить перевод',
    'translation' => 'перевод',
    'Page parsing error. Author fix needed.' 
        => 'Ошибка парсера страницы.  Необходимо редактирование автора.',

    'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec' 
        => 'Янв Фев Мар Апр Мая Июн Июл Авг Сен Окт Ноя Дек',

    'January February March April May June July August '
        . 'September October November December' 
        => 'Январь Февраль Март Апрель Май Июнь Июль Август '
            . 'Сентябрь Октябрь Ноябрь Декабрь',

    'Comment was added sucessfully' => 'Комментарий был успешно добавлен',
    'Comment was deleted sucessfully' => 'Комментарий был успешно удален',
    'Login was successfull' => 'Вход успешно завершен',
    'Logout was successfull' => 'Выход успешно завершен',
    'OpenID login was successfull' => 'Вход по OpenID успешно завершен',
    'Post was added successfully' => 'Запись была успешно добавлена',
    'Post was deleted successfully' => 'Запись была успешно удалена',
    'Post was saved successfully' => 'Запись была успешно сохранена',
    'Tag was deleted sucessfully' => 'Тэг был успешно удален',
    'Page was added successfully' => 'Страница была успешно добавлена',
    'Page was deleted successfully' => 'Страница была успешно удалена',
    'Page was saved successfully' => 'Страница была успешно сохранена',

    '[_1] on post "[_2]"' => '[_1] на запись «[_2]»',

    '[quant,_1,comment]' =>
        '[quant,_1,комментариев,комментарий,комментария]',

    'Powered by [_1]' => 'Крутится на [_1]',
    'This page takes [_1]s to load' 
        => 'Страница сгенерирована за [_1]с'
);

sub quant {
    my ( $s, $quant, $zero, $one, $two ) = @_;

    if ( $quant =~ m/(?:0|1[1-4]|[5-9])$/ox ) {
        return "$quant $zero";
    } elsif ( $quant =~ m/(?:1)$/ox ) {
        return "$quant $one";
    } else {
        return "$quant $two";
    }

    return 0;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
