package Blog::Model::Comment;

use strict;

use base qw/ Blog::RDBO /;

__PACKAGE__->config( name => 'Blog::RDBO::Comment' );

sub list {
    my $self = shift;
    my %args = %{ $_[ 0 ] };

    return $self->manager->get_list( %args );
}

sub recent {
    my ( $self, $limit ) = @_;

    my $comments = $self->manager->get_comments(
        #select  => [ qw/ id key orig_lang t2.i18nid t2.lang t2.istran t2.title / ],
        #sort_by => 'addtime DESC',
        limit   => $limit || 10
    );

    return [ map { { $_->column_value_pairs } } @$comments ];
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
