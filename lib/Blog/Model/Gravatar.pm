package Blog::Model::Gravatar;

use strict;

use base 'CatalystX::Model::Gravatar';

sub COMPONENT {
    my ( $class, $app, $args ) = @_;

    my $config = $app->config->{ 'Model::Gravatar' };
    $class->config( $config ) if $config;

    return $class->NEXT::COMPONENT( $app, $args );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
