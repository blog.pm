package Blog::Model::Page;

use strict;

use base qw/ Blog::RDBO /;

__PACKAGE__->config(
    name    => 'Blog::RDBO::Page',
    manager => 'Blog::RDBO::Page::Manager'
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
