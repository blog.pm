package Blog::Model::Post;

use strict;

use base qw/ Blog::RDBO /;

use Hash::Merge 'merge';

__PACKAGE__->config(
    name    => 'Blog::RDBO::Post',
    manager => 'Blog::RDBO::Post::Manager'
);

sub list {
    my ( $self ) = shift;
    my %args = %{ $_[0] };

    my $cond = merge { sort_by => 'addtime DESC' }, \%args;

    my $objects = $self->manager->get_list( %$cond );

    return [
        map {
            {
                $_->column_value_pairs,
                comment_count => $_->comment_count,
                tags => [ $_->tags ]
            }
        } @$objects
    ];
}

sub recent {
    my ( $self, $limit ) = @_;

    my $posts = $self->manager->get_posts(
        i18n => Blog->context->language,
        select  => [ qw/ id key orig_lang t2.i18nid t2.lang t2.istran t2.title / ],
        sort_by => 'addtime DESC',
        limit   => $limit || 10
    );

    return [ map { { $_->column_value_pairs } } @$posts ];
}

sub newer {
    my ( $self, $addtime ) = @_;

    return $self->manager->get_newer(
        addtime => $addtime,
        select =>
          [ qw/ id key orig_lang t2.i18nid t2.lang t2.istran t2.title / ],
        with_objects => ['post_i18n']
    );
}

sub older {
    my ( $self, $addtime ) = @_;

    return $self->manager->get_older(
        addtime => $addtime,
        select =>
          [ qw/ id key orig_lang t2.i18nid t2.lang t2.istran t2.title / ],
        with_objects => ['post_i18n']
    );
}

sub archive {
    my ( $self, $year, $month, $day ) = @_;

    return $self->manager->get_archive(
        year => $year,
        month => $month,
        day => $day
    );
}

sub archive_month_list {
    my ( $self ) = @_;

    return $self->manager->get_archive_month_list();
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
