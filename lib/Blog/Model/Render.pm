package Blog::Model::Render;

use strict;

use base 'Catalyst::Model';

use vars qw/ $bbcode $ccode /;

use Blog::Parse::BBCode;

sub bbcode {
    my $self = shift;
    my ( $text ) = @_;

    $bbcode ||= Blog::Parse::BBCode->new();

    return $bbcode->render( $text );
}

sub ccode {
    my $self = shift;
    my ( $text ) = @_;

    $ccode ||= Blog::Parse::BBCode->new( deny => 'noparse' );

    return $ccode->render( $text );
}

1;
