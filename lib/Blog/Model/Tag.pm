package Blog::Model::Tag;

use strict;

use base qw/ Blog::RDBO /;

__PACKAGE__->config(
    name    => 'Blog::RDBO::Tag',
    manager => 'Blog::RDBO::Tag::Manager'
);

sub cloud_list {
    my $self = shift;

    return Blog::RDBO::Tag::Manager->get_cloud_list();
    return $self->manager->get_cloud_list();
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
