package Blog::Model::Util;

use strict;
use warnings;

use base 'Catalyst::Model::Adaptor';

__PACKAGE__->config( class => 'Blog::Util' );

1;
