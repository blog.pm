package Blog::NewDB;

BEGIN {
    use FindBin;
    my $db  = "$FindBin::Bin/../blog.db";
    unless ( -f $db ) {
        warn 'Creating new db...';

        require DBI;

        my $dbh = DBI->connect("dbi:SQLite:$db") or die $DBI::errstr;

        $dbh->do( <<SQL );
CREATE TABLE post (
  id INTEGER PRIMARY KEY NOT NULL,
  key character varying NOT NULL,
  orig_lang varchar(2) NOT NULL,
  addtime datetime NOT NULL,
  modtime datetime NOT NULL,
  UNIQUE(key)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE post_i18n (
  i18nid INTEGER PRIMARY KEY NOT NULL,
  post_id INTERGER NOT NULL,
  lang varchar(2) NOT NULL,
  istran int(1) NOT NULL DEFAULT 0,
  title character varying NOT NULL,
  annot text NOT NULL,
  content text NOT NULL
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE tag (
  id INTEGER PRIMARY KEY NOT NULL,
  name character varying NOT NULL,
  UNIQUE(name)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE post_tag_map (
  post_id INTEGER KEY NOT NULL,
  tag_id INTEGER NOT NULL,
  PRIMARY KEY(post_id, tag_id)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE comment (
  id INTEGER PRIMARY KEY NOT NULL,
  post_id INTEGER NOT NULL,
  isauthor INTEGER(1) NOT NULL DEFAULT 0,
  addtime datetime NOT NULL,
  name character varying,
  openid character varying,
  email character varying,
  content text NOT NULL
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE page (
  id INTEGER PRIMARY KEY NOT NULL,
  rank integer NOT NULL DEFAULT 0,
  key character varying NOT NULL,
  orig_lang varchar(2) NOT NULL,
  addtime datetime NOT NULL,
  modtime datetime NOT NULL,
  UNIQUE(key)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE page_i18n (
  i18nid INTEGER PRIMARY KEY NOT NULL,
  page_id INTERGER NOT NULL,
  lang varchar(2) NOT NULL,
  istran int(1) NOT NULL DEFAULT 0,
  title character varying NOT NULL,
  content text NOT NULL
);
SQL

        $dbh->disconnect();

        sleep( 1 );
    }
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
