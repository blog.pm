package Blog::Parse::BBCode;

use strict;
use base 'Parse::BBCode';

use Parse::BBCode::XHTML;

use vars qw($url_re);

sub new {
    my $self = shift;

    $self->SUPER::new(
        {
            tags => {
                Parse::BBCode::XHTML->defaults,
                '' => sub {
                    my $e = Parse::BBCode::escape_html( $_[ 2 ] );
                    $e =~ s/\x0D\x0A/\n/g;
                    $e =~ tr/\x0D\x0A/\n\n/;
                    $e =~ s/(?:\r?\n){2,}/<\/p><p>/gs;
                    $e =~ s/(?:\r?\n)/<br \/>/gs;
                    #$e = $self->auto_link( $e );
                    $e;
                },
                'u'  => '<span style="text-decoration:underline">%s</span>',
                's'  => '<s>%s</s>',
                code => {
                    code => sub {
                        my ( $parser, $attr, $content, $attribute_fallback ) =
                          @_;
                        $content = Parse::BBCode::escape_html( $$content );
                        $content =~ s/^[[:space:]]+//;
                        $content =~ s/[[:space:]]+$//;
                        $attr ||= '';
                        qq|<pre><code class="$attr">$content</code></pre>|;
                    },
                    parse => 0,
                    class => 'block',
                },
                page => {
                    code => \&_tag_page
                },
                post => {
                    code => \&_tag_post
                }
            },
        }
    );
}

sub render {
    my $self = shift;
    my ( $text ) = @_;

    $text =~ s/[[:blank:]]*\[cut\][[:blank:]]*//so;

    my $render = $self->SUPER::render($text);

    return '<p>' . $render . '</p>';
}

my $http_url_re =
q{\b(?:https?|shttp)://(?:(?:[-_.!~*'()a-zA-Z0-9;:&=+$,]|%[0-9A-Fa-f} .
q{][0-9A-Fa-f])*@)?(?:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)} .
q{*[a-zA-Z](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.} .
q{[0-9]+)(?::[0-9]*)?(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f]} .
q{[0-9A-Fa-f])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-} .
q{Fa-f])*)*(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f} .
q{])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*)} .
q{*)?(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])} .
q{*)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*} .
q{)?};

my $ftp_url_re =
q{\bftp://(?:(?:[-_.!~*'()a-zA-Z0-9;&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*} .
q{(?::(?:[-_.!~*'()a-zA-Z0-9;&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)?@)?(?} .
q{:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)*[a-zA-Z](?:[-a-zA-} .
q{Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)(?::[0-9]*)?} .
q{(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*(?:/(?} .
q{:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*(?:;type=[} .
q{AIDaid])?)?(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9} .
q{A-Fa-f])*)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A} .
q{-Fa-f])*)?};

$url_re = "($http_url_re|$ftp_url_re)";

sub auto_link {
    my $self = shift;
    my ( $text ) = @_;

    $text =~ s#$url_re#<a href="$1" rel="nofollow">$1</a>#;

    return $text;
}

sub _tag_internal_uri {
    my ( $key, $model_name ) = @_;

    my $name = lc $model_name;
    my $c    = Blog->context;
    my $object = $c->model( $model_name )->create( key => $key );
    $object->load(
        query       => [ 't2.lang' => $c->language, ],
        with        => [ $name . '_i18n' ],
        select      => [ qw/ key t2.i18nid t2.title / ],
        speculative => 1
    );

    return '' if $object->not_found;

    my $title = $object->i18n->title;
    my $uri = $c->uri_for( "/$name/view", $key );
    qq|<a href="$uri">$title</a>|;
}

sub _tag_page {
    my ( $parser, $attr, $content, $attribute_fallback ) = @_;

    _tag_internal_uri( $$content, 'Page' );
}

sub _tag_post {
    my ( $parser, $attr, $content, $attribute_fallback ) = @_;

    _tag_internal_uri( $$content, 'Post' );
}

1;
