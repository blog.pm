package Blog::RDBO::Comment;

use strict;

use base qw(Blog::DB::Object);

use Rose::DB::Object::Helpers 'column_value_pairs';

use Common::RDBO::Helper::Comment ':all';

__PACKAGE__->meta->setup(
    table => 'comment',

    columns => [
        qw/
          id
          post_id
          isauthor
          email
          name
          openid
          content
          /,
          addtime => { type => 'epoch', default => 'now' }
    ],

    primary_key_columns => [ 'id' ],

    relationships => [
        post => {
            type        => 'many to one',
            class       => 'Blog::RDBO::Post',
            key_columns => { post_id => 'id' }
        }
    ]
);

__PACKAGE__->utf8_columns( qw/ name content / );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
