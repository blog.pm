package Blog::RDBO::Comment::Manager;

use strict;

use base 'Rose::DB::Object::Manager';

use Blog::RDBO::Comment;

sub object_class { 'Blog::RDBO::Comment' }

__PACKAGE__->make_manager_methods( 'comments');

use Common::RDBO::Helper::Comment::Manager ':all';

sub recent {
    my $self = shift;
    my ( $limit ) = @_;

    my $c = Blog->context;

    return $self->get_recent(
        query => [ 't3.lang' => $c->language ],
        select  => [ qw/ id post_id t2.id t2.key t3.i18nid content / ],
        limit   => $limit,
        require_objects => [ 'post.post_i18n' ]
    );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
