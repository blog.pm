package Blog::RDBO::Page;

use strict;

use base qw(Blog::DB::Object::I18N::Static);

__PACKAGE__->meta->setup(
    table => 'page',

    columns => [
        qw/ id key orig_lang /,
        rank => { default => 0 },
        addtime => { type => 'epoch', default => 'now' },
        modtime => { type => 'epoch', default => 'now'}
    ],

    primary_key_columns => [ qw/ id / ],

    unique_key => [ qw/ key / ],

    relationships => [
        page_i18n => {
            type       => 'one to many',
            class      => 'Blog::RDBO::PageI18N',
            column_map => { id => 'page_id' }
        }
    ]
);

__PACKAGE__->utf8_columns( qw/ key / );

__PACKAGE__->meta->column( 'modtime' )->add_trigger(
    event => 'on_save',
    code  => sub {
        my $self  = shift;
        my $value = $self->modtime;

        $self->modtime( time );

    }
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
