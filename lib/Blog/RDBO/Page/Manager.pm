package Blog::RDBO::Page::Manager;

use strict;

use base 'Rose::DB::Object::I18N::Manager';

use Blog::RDBO::Comment;

sub object_class { 'Blog::RDBO::Page' }

__PACKAGE__->make_manager_methods( 'pages');

sub nav {
    my ( $self ) = shift;

    return $self->get_pages(
        i18n => Blog->context->language,
        sort_by => 'rank ASC'
    );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
