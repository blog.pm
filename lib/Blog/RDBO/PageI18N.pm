package Blog::RDBO::PageI18N;

use strict;

use base qw/ Blog::DB::Object::I18N::Translation /;

__PACKAGE__->meta->setup(
    table => 'page_i18n',

    columns => [
        qw/
          i18nid
          page_id
          lang
          istran
          title
          /,
        content => { lazy => 1 }
    ],

    primary_key_columns => [ 'i18nid' ],

    foreign_keys => [
        page => {
            class       => 'Blog::RDBO::Page',
            key_columns => { page_id => 'id' },
            rel_type    => 'one to one',
        },
    ],
);

__PACKAGE__->utf8_columns( qw/ title content / );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
