package Blog::RDBO::Post;

use strict;

use base qw/ Blog::DB::Object::I18N::Static /;

use Common::RDBO::Helper::Post ':all';

__PACKAGE__->meta->setup(
    table => 'post',

    columns => [
        qw/ id key orig_lang /,
        addtime => { type => 'epoch', default => 'now' },
        modtime => { type => 'epoch', default => 'now'}
    ],

    primary_key_columns => [ qw/ id / ],

    unique_key => [ qw/ key / ],

    relationships => [
        post_i18n => {
            type       => 'one to many',
            class      => 'Blog::RDBO::PostI18N',
            column_map => { id => 'post_id' }
        },
        tags => {
            type      => 'many to many',
            map_class => 'Blog::RDBO::PostTagMap',
            map_from  => 'post',
            map_to    => 'tag'
        },
        post_tag_map => {
            type  => 'one to many',
            class => 'Blog::RDBO::PostTagMap',
        },
        comments => {
            type       => 'one to many',
            class      => 'Blog::RDBO::Comment',
            column_map => { id => 'post_id' }
        }
    ]
);

__PACKAGE__->utf8_columns( qw/ key / );

__PACKAGE__->meta->column( 'modtime' )->add_trigger(
    event => 'on_save',
    code  => sub {
        my $self  = shift;
        my $value = $self->modtime;

        $self->modtime( time );

    }
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
