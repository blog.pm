package Blog::RDBO::Post::Manager;

use strict;

use base 'Rose::DB::Object::I18N::Manager';

use Hash::Merge 'merge';
use Common::RDBO::Helper::Post::Manager qw(:all);

sub object_class { 'Blog::RDBO::Post' }

__PACKAGE__->make_manager_methods( 'posts');

sub feed {
    my ( $self ) = @_;

    return $self->get_posts(
        i18n => Blog->context->language,
        sort_by  => 'addtime DESC',
        limit    => 20,
        group_by => 't1.id'
    );
}

sub archive {
    my $self = shift;
    my ( $year, $month, $day ) = @_;

    return $self->get_archive(
        i18n => Blog->context->language,
        year => $year,
        month => $month,
        day => $day
    );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
