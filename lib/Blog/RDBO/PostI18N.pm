package Blog::RDBO::PostI18N;

use strict;

use base qw/ Blog::DB::Object::I18N::Translation /;

__PACKAGE__->meta->setup(
    table => 'post_i18n',

    columns => [
        qw/
          i18nid
          post_id
          lang
          istran
          title
          annot
          /,
        content => { lazy => 1 }
    ],

    primary_key_columns => [ 'i18nid' ],

    foreign_keys => [
        post => {
            class       => 'Blog::RDBO::Post',
            key_columns => { post_id => 'id' },
            rel_type    => 'one to one',
        },
    ],
);

__PACKAGE__->utf8_columns( qw/ title annot content / );

__PACKAGE__->meta->column( 'annot' )->add_trigger(
    event => 'on_save',
    code  => sub {
        my $self  = shift;
        my $value = $self->content;

        if ( $value =~ m/(.*?)\[cut\]/s ) {
            my $annot = $1;
            $annot =~ s/[[:space:]]+$//s;
            $self->annot( $annot );
        } else {
            $self->annot( $value );
        }
    }
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
