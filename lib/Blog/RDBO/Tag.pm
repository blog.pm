package Blog::RDBO::Tag;

use strict;

use base qw(Blog::DB::Object);

use Common::RDBO::Helper::Tag ':all';

__PACKAGE__->meta->setup(
    table => 'tag',

    columns => [
        qw/
          id
          name
          /
    ],

    primary_key_columns => [ 'id' ],

    unique_key => 'name',

    relationships => [
        post_tag_map => {
            type       => 'one to many',
            class      => 'Blog::RDBO::PostTagMap',
            column_map => { id => 'tag_id' }
        },
        posts => {
            type      => 'many to many',
            map_class => 'Blog::RDBO::PostTagMap',
            map_from  => 'tag',
            map_to    => 'post'
        }
    ]
);

__PACKAGE__->utf8_columns( qw/ name / );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
