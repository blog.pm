package Blog::Util;

use strict;

sub new {
    my $class = shift;

    my $self = {};

    bless $self;

    return $self;
}

sub mon2name {
    my ( $self, $mon, $short ) = @_;

    return unless defined $mon && ( $mon >= 0 && $mon < 12 );

    $short ||= '';
    my $abbr = $self->{ "_helper_util_mon2name_abbr$short" };

    unless ( $abbr ) {
        my @abbr =
          split(
            ' ',
            $self->{ c }->loc(
                $short
                ? 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'
                : 'January February March April May June July August '
                  . 'September October November December'
            )
          );

        $abbr = $self->{ "_helper_util_mon2name_abbr$short" } = \@abbr;
    }

    return $abbr->[ $mon ];
}

sub datefmt {
    my ( $self, $dt ) = @_;

    return unless ref $dt;

    #my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
      #localtime( $date );

    return sprintf(
        qq#<span class="dateformat"><span class="date">%02d %s, %d</span>#,
        $dt->day, $dt->month_name, $dt->year
    );
    #return sprintf(
        #qq#<span class="dateformat"><span class="date">%02d %s, %d</span>#,
        #$dt->day, $self->mon2name( $dt->month - 1, 1 ), $dt->year
    #);
}

sub datediff {
    my ( $self, $addtime, $modtime ) = @_;

    return $modtime->delta_days( $addtime )->days;
}

sub uuid {
    my ( $self ) = @_;

    my $uuid = int( rand( 9 ) ) + 1;
    foreach ( 1 .. 5 ) {
        $uuid .= int( rand( 10 ) );
    }

    return $uuid;
}

sub cut {
    my ( $self, $text, $maxlength ) = @_;

    $maxlength ||= 50;

    if ( $text =~ m/^<p>(.*?)<\/p>/os ) {
        $text = $1;
    }
  
    $text =~ s/<.*?>//g;

    my $toolong = 1 if length $text > $maxlength;

    if ( $toolong ) {
        $text = substr( $text, 0, $maxlength );
        $text =~ s/[^[:blank:]]+$//o;
        $text .= '[...]';
    }

    return $text ? $text : '[...]';
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
