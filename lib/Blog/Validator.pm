package Blog::Validator;

use strict;

sub new {
    my ( $class, $params ) = @_;

    my $self = bless { params => $params, fields => {}, errors => {} }, $class;

    $self->init_fields();

    return $self;
}

sub init_fields { }

sub add_fields {
    my $self = shift;

    $self->{ fields } ||= {};

    $self->{ fields } = { %{ $self->{ fields } }, @_ };
}

sub error {
    my ( $self, $field, $error ) = @_;

    return unless $field;

    if ( $error ) {
        $self->{ errors }->{ $field } ||= [];

        push @{ $self->{ errors }->{ $field } }, $error;

        return;
    }

    return $self->{ errors }->{ $field };
}

sub errors {
    my ( $self ) = @_;

    return $self->{ errors };
}

sub is_valid {
    my ( $self ) = @_;

    #use Data::Dumper;

    #die Dumper($self->{ fields });

    $self->clean_params();

    FIELD:
    while ( my ( $field, $value ) = each %{ $self->{ fields } } ) {
        if ( !defined $self->param( $field ) || $self->param( $field ) eq '' ) {
            $self->error( $field, 'Required' ) if $value->{ required };
            next FIELD;
        }

        RULE:
        while ( my ( $rule, $arg ) = each %$value ) {
            next RULE if $rule eq 'required';
            my $method = "check_$rule";
            if ( $self->can( $method ) ) {
                $self->$method( $field, $arg );
            }
        }
    }

    #die Dumper $self->errors;

    return keys %{ $self->errors } ? 0 : 1;
}

sub field {
    my ( $self, $name ) = @_;

    return unless $name;

    return $self->{ fields }->{ $name };
}

sub param {
    my ( $self, $name ) = @_;

    return unless $name;

    return $self->{ params }->{ $name };
}

sub params {
    my ( $self ) = @_;

    return $self->{ params };
}

sub clean_params {
    my ( $self ) = @_;

    foreach my $param ( keys %{ $self->params } ) {
        delete $self->{ params }->{ $param }
          unless exists $self->{ fields }->{ $param };
    }
}

sub check_re {
    my ( $self, $name, $re ) = @_;

    $self->error( $name, 'Invalid input' )
      unless $self->param( $name ) =~ $re;
}

sub check_email {
    my ( $self, $name, $email ) = @_;

    $self->error( $name, 'Invalid email' )
      unless $self->param( $name ) =~ m/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]?\.)+[a-zA-Z]{2,9})$/;
}

sub check_in {
    my ( $self, $name, $in ) = @_;

    $self->error( $name, 'Invalid input' )
      unless grep { $self->param( $name ) eq $_ } @$in;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
