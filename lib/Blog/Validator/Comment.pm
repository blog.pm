package Blog::Validator::Comment;

use base 'Blog::Validator';

use strict;

sub init_fields {
    my ( $self ) = @_;

    $self->add_fields(
        post_id => { re => qr/^\d+$/, required => 1 },
        content => { required => 1 },
        name => {},
        email => { email => 1 }
    );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
