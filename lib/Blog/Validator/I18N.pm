package Blog::Validator::I18N;

use strict;

use base 'Blog::Validator';

use Blog;

sub init_fields {
    my ( $self ) = @_;

    my $c = Blog->context;
    $self->add_fields( language =>
          { required => 1, in => [ keys %{ $c->config->{ languages } } ] } )
      unless $c->action =~ 'edit';
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
