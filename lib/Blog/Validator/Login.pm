package Blog::Validator::Login;

use base 'Blog::Validator';

use strict;

sub init_fields {
    my ( $self ) = @_;

    $self->add_fields(
        name     => { required => 1 },
        password => { required => 1 }
    );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
