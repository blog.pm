package Blog::Validator::Page;

use strict;

use base 'Blog::Validator::I18N';

use Blog::RDBO::Page::Manager;

sub init_fields {
    my ( $self ) = @_;

    $self->add_fields(
        key     => { re => qr/^[[:alnum:]-]+$/, required => 1 },
        rank    => { re => qr/^\d+$/ },
        title   => { required => 1 },
        content => { required => 1 }
    );

    $self->SUPER::init_fields();
}

sub is_valid {
    my ( $self ) = shift;
    my %opt = @_;

    my $ok = $self->SUPER::is_valid();
    return $ok unless $ok;

    return 1 if $self->param('key') && $self->param( 'key' ) eq $opt{unique_except};

    my $exists =
      Blog::RDBO::Page::Manager->get_pages_count(
        query => [ key => $self->param( 'key' ) ] );

    if ( $exists ) {
        $self->error( 'key', 'Unique' );
        return 0;
    }

    return 1;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
