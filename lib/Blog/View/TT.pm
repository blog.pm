package Blog::View::TT;

use warnings;
use strict;

use base 'Catalyst::View::TT';
#use Template::Constants qw(:debug);

use HTTP::Date;
use Encode 'encode_utf8';

__PACKAGE__->config(
    {
        COMPILE_DIR  => Blog->path_to( 'tmp' ),
        COMPILE_EXT  => '.pl',
        CATALYST_VAR => 'c',
        INCLUDE_PATH => [
            Blog->path_to( 'templates', 'lib' ),
            Blog->path_to( 'templates', 'src', Blog->config->{ style })
        ],
        TEMPLATE_EXTENSION => '.tt2',
        TRIM               => 1,
        PRE_PROCESS        => 'config',
        WRAPPER            => 'wrapper',
        ERROR              => 'error.tt2',
        TIMER              => 0,
        #DEBUG              => DEBUG_UNDEF,
        EVAL_PERL          => 1,
        FILTERS            => {
            bbcode => \&bbcode,
            ccode  => \&ccode,
            cut    => \&cut,
            encode_utf8 => \&encode_utf8
        }
    }
);

sub process {
    my ( $self ) = shift;
    my ( $c ) = @_;

    my $feed = $c->stash->{ feed };
    if ( $feed ) {
        if ( keys %$feed ) {
            $feed->{ description } = $feed->{ description };
            if ( scalar @{ $feed->{ items } } ) {
                $feed->{ lastBuildDate } =
                  time2str( $feed->{ items }->[ 0 ]->{ pubDate } );

                foreach my $item ( @{ $feed->{ items } } ) {
                    $item->{ pubDate } = time2str( $item->{ pubDate } );
                }
            }
        } else {
            $feed->{ items } = [];
        }

        $c->stash->{ template } = 'feed';
    }

    $self->next::method(@_);
}

sub bbcode {
    my $text = shift;

    Blog->context->model( 'Render' )->bbcode( $text );
}

sub ccode {
    my $text = shift;

    Blog->context->model( 'Render' )->ccode( $text );
}

#sub ccode {
#    my $text = shift;
#
#    return unless $text;
#
#    #$text =~ s/&/&amp;/go;
#    $text =~ s/</&lt;/go;
#    $text =~ s/>/&gt;/go;
#
#    $text =~ s/\x0D\x0A/\n/g;
#    $text =~ tr/\x0D\x0A/\n\n/;
#
#    $text =~ s{(?<!\n)(\n)(?!\n)}{<br />\n}g;
#    $text = "<p>"
#      . join( "</p>\n\n<p>\n", split( /(?:\r?\n){2,}/, $text ) )
#      . "</p>\n";
#
#    $text;
#}

sub cut {
    my ( $text, $maxlength ) = @_;

    Blog->context->model('Util')->cut( $text, $maxlength);
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

