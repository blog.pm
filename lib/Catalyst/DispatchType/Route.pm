package Catalyst::DispatchType::Route;

use strict;
use base qw/Catalyst::DispatchType/;
use Text::SimpleTable;
use URI;

use Catalyst::DispatchType::RouteMapper;

=head1 NAME

Catalyst::DispatchType::Route - RoR Route DispatchType

=head1 SYNOPSIS

See L<Catalyst>.

=head1 DESCRIPTION

=head1 METHODS

=head2 $self->list($c)

Debug output for Route dispatch points

=cut

sub list {
    my ( $self, $c ) = @_;
    my $paths = Text::SimpleTable->new( [ 35, 'Route' ], [ 36, 'Private' ] );
    foreach my $path ( sort keys %{ $self->{routes} } ) {
        my $display_path = $path eq '/' ? $path : "/$path";
        $paths->row( $display_path, "/" . $self->{routes}->{$path} );
    }
    $c->log->debug( "Loaded Route actions:\n" . $paths->draw . "\n" )
      if ( keys %{ $self->{routes} } );
}

=head2 $self->match( $c, $path )

For each action registered to this exact path, offers the action a chance to
match the path (in the order in which they were registered). Succeeds on the
first action that matches, if any; if not, returns 0.

=cut

sub match {
    my ( $self, $c, $path ) = @_;

    return 0 if @{ $c->req->args };

    $path ||= '/';
    use Data::Dumper;

    #$c->log->debug('path=' . $path);
    #$c->log->debug('ehre' . Dumper($self->{routes} ));

    my $mapper = $self->{mapper};
    if (my $route = $mapper->parse($path)) {
        #$c->log->debug('FOUDN!!! route=' . $route);

        if (my $action = $self->{routes}->{$route}) {;
            #$c->log->debug('ACTION!');
            $c->req->action($path);
            $c->req->match($path);
            $c->action($action);
            $c->namespace( $action->namespace );

            if (my $params = $mapper->getParams()) {
                while (my($key, $val) = each %$params ) {
                    $c->req->param( $key => $val);
                }
            }

            return 1;
        }
    }

    return 0;
}

=head2 $self->register( $c, $action )

Calls register_path for every Path attribute for the given $action.

=cut

sub register {
    my ( $self, $c, $action ) = @_;

    my @register = @{ $action->attributes->{Route} || [] };

    $self->register_route( $c, $_, $action ) for @register;

    return 1 if @register;
    return 0;
}

=head2 $self->register_path($c, $path, $action)

Registers an action at a given path.

=cut

sub register_route {
    my ( $self, $c, $route, $action ) = @_;

    #$c->log->debug( 'path=' . $route . ' action=' . $action );

    $self->{mapper} ||= Catalyst::DispatchType::RouteMapper->new();

    my $mapper = $self->{ mapper };

    $self->{routes} ||= {};

    $route = $action->namespace . '/' . $route unless $route =~ s/^\///o;

    my $map = {};

    my @vars = split('/', $route);
    my @new_vars = ();
    foreach my $var ( @vars ) {
        if ( $var =~ m/^\:(.*?)(?:\|(.*?)\|)?(\=(\?)?(.*))?$/o ) {
            my $name       = $1;
            my $constraint = $2;
            my $extra      = $3;

            $var = ":$name";

            $map->{ constraints }->{ $name } = $constraint if $constraint;

            if ( $extra ) {
                my $isdefault = $4;
                my $val       = $5;

                $map->{ defaults }->{ $name } = $val if $isdefault && $val;
                $map->{ params }->{ $name } = $isdefault
                                                ? undef
                                                : $val || undef;
            }
        }

        push @new_vars, $var;
    }

    $route = join('/', @new_vars);
    $self->{routes}->{$route} = $action;

    $map->{ path } = $route;
    $mapper->addMap($map);

    #use Data::Dumper;

    #$c->log->debug(Dumper($map));

    return 1;
}

=head2 $self->uri_for_action($action, $captures)

get a URI part for an action; always returns undef is $captures is set
since Path actions don't have captures

=cut

sub uri_for_action {
    my ( $self, $action, $captures ) = @_;

    return undef if @$captures;

    if (my $paths = $action->attributes->{Path}) {
        my $path = $paths->[0];
        $path = '/' unless length($path);
        $path = "/${path}" unless ($path =~ m/^\//);
        $path = URI->new($path)->canonical;
        return $path;
    } else {
        return undef;
    }
}

=head1 AUTHOR

vti

=head1 COPYRIGHT

This program is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
