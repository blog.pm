package Catalyst::DispatchType::RouteMapper;
use strict;



sub new
{
    my $class = shift;
    my ($config) = @_;

    my $self = {};

    bless $self, $class;

    $self->{config} = $config || [];

    return $self;
}

sub _parse_init
{
    my $self = shift;

    $self->{ _params} = undef;
    $self->{ _params_len} = undef;
    $self->{ _route } = undef;
}

sub parse
{
    my $self = shift;
    my ($path_info) = @_;

    $self->_parse_init();

    die "no path_info provided\n" unless $path_info;

    $path_info =~ s/\/$// unless $path_info eq '/';

    my $config = $self->getConfig();

    foreach my $route (@$config) {
        last if $self->_checkStarRoute($route);

        last if $self->_checkRoute($path_info, $route);
    }

    my $found_route;
    if ($found_route = $self->{ _route }) {
        $self->_setRouteDefaults($found_route);

        $self->_setRouteParams($found_route);

        return $found_route->{ path };
    }

    return 0;
}

sub _checkStarRoute
{
    my $self = shift;
    my ($route) = @_;

    if ($route->{path} eq '*') {
        $self->{ _route } = $route;
        return 1;
    }

    return 0;
}

sub _checkRoute
{
    my $self = shift;
    my ($path_info, $route) = @_;

    $self->{params} = undef;

    $self->{_params} ||= [split('/', $path_info)];
    $self->{_params_len} ||= scalar @{$self->{_params}};

    my $params_len = $self->{_params_len};

    $route->{path} =~ s/^\///;

    my @route_params = split('/', $route->{path});
    $self->{ _route_params_len } = @route_params;

    return
      unless $self->_checkParamCount($route);

    foreach my $param (@{ $self->{_params}}) {
        my $route_param = shift @route_params;

        if ($route_param =~ s/^://) {
            return
              unless $self->_checkRouteParamConstraint($param, $route,
                $route_param);

            $self->{params}->{$route_param} = $param;
        } elsif (uc $route_param ne uc $param) {
            return;
        }
    }

    $self->{ _route } = $route;

    return 1;
}

sub _checkParamCount
{
    my $self = shift;
    my ($route) = @_;

    my ($min_len, $max_len);

    my $params_len = $self->{ _params_len };
    my $route_params_len = $self->{ _route_params_len };

    $min_len = $max_len = $route_params_len;

    foreach my $key (keys %{$route->{params}}) {
        if (!defined $route->{params}->{$key}) {
            $min_len--;
        }
    }

    return $params_len >= $min_len && $params_len <= $max_len ? 1 : 0;
}

sub _checkRouteParamConstraint
{
    my $self = shift;
    my ($param, $route, $route_param) = @_;

    if ($route->{constraints}
        && (my $regexp = $route->{constraints}->{$route_param}))
    {
        return $param =~ m/^$regexp$/x;
    }

    return 1;
}

sub _setRouteDefaults
{
    my $self = shift;
    my ($route) = @_;

    return unless $route && $route->{defaults};

    while (my ($key, $val) = each %{$route->{defaults}}) {
        $self->{params}->{$key} = $val
          if !defined $self->{params}->{$key}
          && defined $val;

    }
}

sub _setRouteParams
{
    my $self = shift;
    my ($route) = @_;

    if ($route) {
        while (my ($key, $val) = each %{$route->{params}}) {
            $self->{params}->{$key} = $val if defined $val;
        }
    }
}

sub addMap
{
    my $s = shift;

    my $map = ref $_[0] eq 'HASH' ? $_[0] : {@_};

    push @{$s->{config}}, $map;

    return 1;
}

sub getConfig
{
    my $self = shift;

    return $self->{ config };
}

sub getParams
{
    my $self = shift;

    return $self->{params};
}

1;
