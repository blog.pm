use strict;
use Test::More tests => 17;

BEGIN { use_ok 'Test::WWW::Mechanize::Catalyst', 'Blog' }

my $mech = Test::WWW::Mechanize::Catalyst->new;

$mech->get_ok('/');
$mech->page_links_ok();

$mech->get_ok('/login');
$mech->page_links_ok();

$mech->get_ok('/login_openid');
$mech->page_links_ok();

$mech->get_ok('/post');
$mech->page_links_ok();

$mech->get_ok('/post/archive');
$mech->page_links_ok();

$mech->get_ok('/post/feed');
$mech->page_links_ok();

$mech->get_ok('/tag');
$mech->page_links_ok();

$mech->get_ok('/comment/feed');
$mech->page_links_ok();
