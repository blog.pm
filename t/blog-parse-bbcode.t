#!perl -T

#use Test::More tests => 2;
use Test::More 'no_plan';

BEGIN {
    use_ok( 'Blog::Parse::BBCode' );
}

my $bbc = Blog::Parse::BBCode->new();

is( $bbc->render('1 > 1'), '<p>1 &gt; 1</p>', 'Escaping' );

is( $bbc->render('1 & 1'), '<p>1 &amp; 1</p>', 'Escaping' );

is( $bbc->render( '[i]italic[/i]' ), '<p><i>italic</i></p>', 'Italic' );

is( $bbc->render( '[b]bold[/b]' ), '<p><b>bold</b></p>', 'Bold' );

is( $bbc->render( '[u]underlined[/u]' ),
    '<p><span style="text-decoration:underline">underlined</span></p>',
    'Underlined' );

is( $bbc->render( '[s]strike[/s]' ), '<p><s>strike</s></p>', 'Strike' );

is( $bbc->render( '[i][u][s][b]mixing up[/b][/s][/u][/i]' ),
    '<p><i><span style="text-decoration:underline"><s><b>mixing up</b></s></span></i></p>'
    , 'Mixing up');

is( $bbc->render( '[hr]' ), '<p><hr /></p>', 'Hr' );

is( $bbc->render( '[url]http://foo.com[/url]' ),
    '<p><a href="http://foo.com" rel="nofollow">http://foo.com</a></p>', 'Url' );

is( $bbc->render( '[url=http://foo.com]Foo[/url]' ),
    '<p><a href="http://foo.com" rel="nofollow">Foo</a></p>', 'Url' );

is( $bbc->render( '[img]http://foo.com/bar.jpg[/img]' ),
    '<p><img src="http://foo.com/bar.jpg" alt="[http://foo.com/bar.jpg]" title="http://foo.com/bar.jpg" /></p>', 'Image' );

is( $bbc->render( '[url=http://foo.com][img]http://foo.com/bar.jpg[/img][/url]'),
    '<p><a href="http://foo.com" rel="nofollow"><img src="http://foo.com/bar.jpg" alt="[http://foo.com/bar.jpg]" title="http://foo.com/bar.jpg" /></a></p>', 
    'Image inside url');

is( $bbc->render( '[code=html][b]bold[/b][/code]' ),
    '<p><pre><code class="html">[b]bold[/b]</code></pre></p>', 'Code' );

is( $bbc->render( '[list][*]one[*]two[/list]' ),
    '<p><ul><li>one</li><li>two</li></ul></p>', 'List' );

#is( $bbc->render( '[left]foo[/left]' ),
#    '<p><div style="text-align:left">foo</div></p>', 'Left aligned text' );
#
#is( $bbc->render( '[center]foo[/center]' ),
#    '<p><div style="text-align:center">foo</div></p>', 'Center aligned text' );
#
#is( $bbc->render( '[right]foo[/right]' ),
#    '<p><div style="text-align:right">foo</div></p>', 'Right aligned text' );
#
#is( $bbc->render( '[float=left][img]http://foo.com/bar.jpg[/img][/float]' ),
#    '<p><div class="float-left"><img src="http://foo.com/bar.jpg" alt="" /></div></p>', 'Floating left image' );
#
is( $bbc->render( '[color=red]red[/color]' ), '<p><span style="color: red">red</span></p>', 'Color');

is( $bbc->render("text\n\nwith\nbr"), '<p>text</p><p>with<br />br</p>' );

is( $bbc->render('http://example.com?q=abc&action=add'),
    '<p><a href="http://example.com?q=abc&amp;action=add" rel="nofollow">http://example.com?q=abc&amp;action=add</a></p>' );

is( $bbc->render('ftp://ftp.example.com')
        , '<p><a href="ftp://ftp.example.com" rel="nofollow">ftp://ftp.example.com</a></p>');

is( $bbc->render('[cut]'), '<p></p>');

# error checking

is($bbc->render('[b=foo]'), '<p>[b=foo]</p>');

is($bbc->render('[url]abc'), '<p>[url]abc</p>');
